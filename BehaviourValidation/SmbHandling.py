import os, sys
from Shellprocess import *

""" function of this folder helps to construct smb files and to handle use of totembionet in differents contexts (interection, csv creation). """


def smb_file_creation2(path, name, envar, var, reg, para, fixed_var_handled, final_FAIRCTL, init_var):
    """
    it write a smb file with needed information
    :param path: path where file will be write
    :param name: number of matrix line
    :param envar: str of all instantiated envar
    :param var: str of all instantiated envar
    :param para: str of all inital parameters coming from PS
    :param fixed_var_handled: dict of all str info to put on  a smb file
    :param final_FAIRCTL: FAIRCTL with premisse in str
    """
    smb = open(path + name + '.smb', "w")
    # print("a")
    smb.write('\nENV_VAR\n\n')
    smb.write(envar)
    smb.write('\nVAR\n\n')
    smb.write(var)
    smb.write('\n\nREG\n\n')
    smb.write(reg + '\n')
    if fixed_var_handled != {}:
        smb.write(fixed_var_handled['reg'])
        if init_var != "":
            smb.write('\n\nINIT\n\n')
            smb.write(init_var + ";\n")
        smb.write('\n\nPARA\n\n')
        smb.write(para)
        smb.write('\n')
        if fixed_var_handled['para']:
            smb.write(fixed_var_handled['para'])
        # TODO: add CTL / HOARE possibiliies
        # if final_CTL != "":
        #    smb.write(final_FAIRCTL+";")
        # smb.write('\n\nCTL\n\n')
        #    smb.write(final_CTL + ";")
        smb.write('\n\nFAIRCTL\n\n')
        if final_FAIRCTL != "":
            smb.write(final_FAIRCTL + ";")
        else:
            smb.write(envar)
    else:
        if init_var != "":
            # print("a")
            # print(init_var)
            smb.write('\n\nINIT\n\n')
            smb.write(init_var + ";\n")
        smb.write('\n\nPARA\n\n')
        smb.write(para)
        # TODO: add CTL / HOARE possibiliies
        # if final_CTL != "":
        #    smb.write(final_FAIRCTL+";")
        # smb.write('\n\nCTL\n\n')
        #    smb.write(final_CTL + ";")
        smb.write('\n\nFAIRCTL\n\n')
        if final_FAIRCTL != ";":
            smb.write(final_FAIRCTL + ";")
        else:
            smb.write(envar)
    smb.write('\n\nEND\n\n')
    smb.close()
    ok_write = "Smb file n° " + str(name) + " has been written"
    #print(ok_write)
    return (ok_write)


def l_smb_files_creation2(path, d_ig, d_smbs_complete_ctl, d_smbs_envar, d_smbs_fixed_var, l_para, d_init):
    """
    it creates all smb files which need to be tested.
    :param path: path where file will be created :param d_ig: d_IG: dict of VAR reg and envar  in list
    :param d_smbs_complete_ctl: dict with line of the V-PM as key and logical formula  associated  to complete CTL bloc
                                of a biological context (smb) in str as value
    :param d_smbs_envar: dict with all envars ranked by the corresponding line number in V-PM
    :param d_smbs_fixed_var: dict with line as keys and str as
    :param l_para: parameter list coming from para.txt file
    :return: l_smbs_files :list of all path+name of files smb to test
    """
    #print(d_smbs_complete_ctl)
    #print(path)
    #print("smbfile")
    if os.path.exists(path):
        shutil.rmtree((path + 'smbFiles/'), ignore_errors=True)
    os.mkdir(path + 'smbFiles/')
    l_smbs_files = []
    para = "\n".join(l_para) + '\n'
    var = "\n".join(d_ig['var'])
    reg = "\n".join(d_ig['reg'])
    num = 1
    while num <= len(d_smbs_envar):
        final_CTL = d_smbs_complete_ctl[num]
        if d_smbs_complete_ctl[num] !='':
            #print(str(num) +"a")
            name = 'bio_cont_' + str(num)
            envar = d_smbs_envar[num]
            #final_CTL = d_smbs_complete_ctl[num]
            if num in d_smbs_fixed_var.keys():  # gestion si pas de fixed var
                d_one_fixed_var = d_smbs_fixed_var[num]
            else:
                d_one_fixed_var = {}
            if num in d_init.keys():  # gestion si pas de fixed var
                one_init_var = d_init[num]
            else:
                one_init_var = ""
            smb_file_creation2(path + 'smbFiles/', name, envar, var, reg, para, d_one_fixed_var, final_CTL, one_init_var)
            l_smbs_files.append(name)
        num += 1
    return l_smbs_files


def error_totem_treatment(str_out_totem,str_error_totem):
    """
    defines the error in totembionet input files creted
    Args:
        str_out_totem:  gives the output of totembionet in str for smfile created
        str_error_totem: gives the error output of totembionet n str for smbfile created
    """
    l_out = str_out_totem.replace('\\r','').split('\\n')
    l_error = str_error_totem.replace('\\r','').split('\\n')

    print("\n*************************************************************************" +
          "\n*** \t Totembionet output files for each environment creation fail \t ***\n" +
          "***************************************************************************")
    print("\n\nTotembionet output-error:\n\n")
    for line in l_out:
        if 'line' in line:
            print(line + '\n')
    for line in l_error:
        if 'line' in line:
            print(line + '\n')
    print("\n\nEND OF THE SCRIPT: please correct you're inputs files")


def smb_convert_to_csv_results(l_smbs_files,path_context):
    from time import sleep
    from tqdm import tqdm
    """
    its use totembionet to convert smb into csv file
    :param l_smbs_files : list of smb files
    :return:csv_file_list : list of str name of csvfiles created
    """
    #rm_dir_py("./../modelingcontext/out_files/")
    #os.mkdir('./../modelingcontext/out_files/')
    #rm_dir_py("./../modelingcontext/csv_files/")
    #os.mkdir('./../modelingcontext/csv_files/')
    rm_dir_py(path_context+"csv_files/")
    os.mkdir(path_context+'csv_files/')
    i = 0
    # print(l_smbs_files)
    for smbfile in tqdm(l_smbs_files):
        current_path = os.getcwd()
        #path = '../modelingcontext/smbFiles/' + smbfile + '.smb'
        #print("ttest")
        #print(path_context)
        #print(smbfile)
        path = path_context +'smbFiles/'+ smbfile + '.smb'
        a, b = run_Totembionet("-csv -verbose 2", path)
        #print(a,b)
        try:
            #os.remove("./../modelingcontext/smbFiles/" + smbfile + "-MDD.csv")
            #shutil.move("./../modelingcontext/smbFiles/" + smbfile + ".csv",
            #            "./../modelingcontext/csv_files/" + smbfile + ".csv")
            #os.remove("./../modelingcontext/smbFiles/" + smbfile + ".out")
            #os.remove("./../modelingcontext/smbFiles/" + smbfile + ".smv")
            os.remove(path_context+"smbFiles/" + smbfile + "-MDD.csv")
            shutil.move(path_context+"smbFiles/" + smbfile + ".csv",
                        path_context+"csv_files/" + smbfile + ".csv")
            os.remove(path_context+"smbFiles/" + smbfile + ".out")
            os.remove(path_context+"smbFiles/" + smbfile + ".smv")
        except:
            error_totem_treatment(str(a),str(b))  # call to function to obtain readable error from totembionet
            sys.exit()

    csv_files_list = os.listdir(path_context+"csv_files/")
    print("csvfile writen")
    return csv_files_list


def vsc_intersection_union(vscfile_reperotry,exrepertory,matrix_name,path_context):
    """
    it calculates intersection between csv result files
    :param type:
    :param vscfile_reperotry:
    :return: String with the results of the program
    """
    a, b = run_Totembionet("-intersection", vscfile_reperotry)
    res=os.path.abspath(exrepertory)

    if os.path.exists(path_context+'csv_files/intersectionOK.csv'):
        shutil.move(path_context+'csv_files/intersectionOK.csv', exrepertory+'/'+matrix_name+'_models.csv')
        return ("Find models in "+ res+'/'+matrix_name+'_models.csv')
    else:
        return ("No models For the RN and biologcal properties given ")
