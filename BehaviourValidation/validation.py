import os
import shutil
import sys
import argparse, pathlib

# todo: penser a utiliser le
# https://www.tutorialspoint.com/python/python_command_line_arguments.html pour faire python arguments
# faire un lancement avec arguments  noms de fichiers puis test de trouver les fichiers si .graphml alors premiere trad sinon smb.
# sys.path.append(os.path.abspath("BehaviourValidation"))
# sys.path.append(os.path.abspath(""))
from os.path import basename


sys.path.append(os.path.abspath("."))
sys.path.append(os.path.abspath(".."))

from FivxHandle import *
from PhenotypeHandling import *
from SmbHandling import l_smb_files_creation2, smb_convert_to_csv_results, vsc_intersection_union


def validation_steps(VPM,path_results, path_to_intput_file,path_context):
    """

    Args:
        VPM:  phenotypic matrix in list of line
        path_results: path where result is stored
        path_to_intput_file: path to smbfile that gives the regulatory network with partially identified parameters

    Returns: either error in creating  output or returns the path to the output .

    """
    l_para,d_ig2 = smb_data_storage(path_to_intput_file)
    # matrix storage editing
    d_FixVar, d_EnVar, d_InitVar = b_context_separation(VPM['context'])  # matrix storage voir ce qu'elle fait
    d_EnVar, d_sys = b_context_EnVar_sep_sys(VPM['context'])
    # formula storage
    # todo: si il y a des formules opereteurs ajoutés autre que CTL avec MACRO

    d_CTL, d_CTL_id = phenotype_translation_CTL(VPM['phenotype'])
     #print(d_CTL)
    # condition treatment
    d_smbs_fixed_var = {}
    d_premise = {}
    if d_sys != {}:
        d_InitVar, d_FixVar = b_sys_separation(d_sys)
        # if there is init var in the context
        #print('I')
        d_premise = init_var_translation_to_premise(d_InitVar)
        # fix handling
    if d_FixVar != {}:
        #print('F')
        d_smbs_fixed_var = fixed_var_to_all_vpm(d_FixVar, l_para)

    # Smbfiles Creation
    envar_with_fix = envar_to_all_vpm(d_EnVar, d_smbs_fixed_var)
    l_smbs_files = l_smb_files_creation2(path_context, d_ig2, d_CTL, envar_with_fix, d_smbs_fixed_var, l_para, d_premise)
    vscfile_reperotry = path_context+ 'csv_files'

    # creation of CSV outputs for each smb file
    csv_files_list = smb_convert_to_csv_results(l_smbs_files,path_context)
    if len(csv_files_list) != len(l_smbs_files):
        return(print("****Error in the Csv fille creation for environments****"))
    else:
        matrix_name = basename(Validation_matrix)[:-4]
        return (vsc_intersection_union(vscfile_reperotry,path_results,matrix_name,path_context))


def file_choices(choices, fname):
    """
    parse the right extension file
    args:
        choices : is a list of correct input extension
    Returns:fname: the path to file to open in the script
    """
    ext = os.path.splitext(fname)[1][1:]
    if ext not in choices:
        parser.error("file doesn't end with {}".format(choices))
    return fname

# Main
#argument parsing ##
####################

parser = argparse.ArgumentParser()
parser.add_argument('smbfile_path', type=lambda s: file_choices(("smb"), s))
parser.add_argument('csv_matrix_path', type=lambda s: file_choices(("csv"), s))
parser.add_argument("-v", "--verbose", action="store_true",
                    help="keep temporary files")
args = parser.parse_args()
#intermediate folder creation
path_context='./modelingcontext/'
if not os.path.exists(path_context):
    os.mkdir(path_context)

#convert args to usable input of the script
input_SMBfile_path = args.smbfile_path #input_SMBfile_path = "./../example/Pseudmonas/smfile.smb"
Validation_matrix = args.csv_matrix_path #Validation_matrix = "./../example/Pseudmonas/V_test5.csv"

#prepation of the result storage path
# Results are stored in Csv file registered in the same place as the input matrix
len_name_matrix = len(os.path.basename(Validation_matrix)) #give the lenth of the name
path_results=str(Validation_matrix)[0:-(len_name_matrix)] #give the path where results are stored

# GreenBioNet execution #
#########################

#datastorage from the matrix
VPM = vpm_data_storage(Validation_matrix, ",")
#whole model validation steps
print(validation_steps(VPM, path_results, input_SMBfile_path,path_context))

# args -v dealing : temporary files #
# if verbose  temporary file preservation #
if args.verbose:
    if os.path.isdir(os.path.dirname(args.csv_matrix_path[:-4]+"files/")):
        shutil.rmtree(os.path.dirname(args.csv_matrix_path[:-4]+"files/"))
    shutil.copytree(path_context, args.csv_matrix_path[:-4]+"files/")
    shutil.rmtree(path_context)
    print("Temporary files are written in modeling context folder")
else:
    shutil.rmtree(path_context)
