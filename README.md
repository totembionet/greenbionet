Before using the given module, you need to install TOtembionet correctly ( TotemBioNet commands are avaliable from anywhere in the computer).

The GreenBioNet directory contains:
    -Behaviour validation  directory which contains all ".py" file,
    -example directory which contains all refered examples concerning the tool
    -greenbionet shell-script to lunch 2 arguments files (an smfile and a matrixfile)
    
In the example directory there is:
	-the directory cell_metabolism  
	
Booth are examples explainned in a submitted version of "A Phenotypic Matrix for Greening QualitativeRegulatory Retwork with Environments" written by Laetitia gibart, Hélène Collavizza and Jean-Paul Comet. Each directory contains:
- An  smbfile
- An Phenotypic matrix in csv
- A file called matrix_name_Models.csv

the first two are inputs of the tool and the result called as the name of the matrix with "_models" added. If you want to test the tool pleas use those two examples. The cell metabolism take a while ( more thant 30 minutes) so if you want a quick start please use the Pseudomonas example
 

To use the validation function, you need to create different input files :
	- A Smbinput file in ".smb" containing informations of the regulatory network to test written with TotemBioNet syntax. Particularly the input file should contain a VAR block, a REG block and a PARA block. VAR and REG block refers to the influence graph. the PARA block can be empty if there is no biological knowledge about it, but the word PARA should apprear.
	
     - A Pehnotypic martix file in ".csv" that represents a visual version of the biological proparties to verify. This table is then translated into CTL formulas. 
. 

For the Phenotypic matric file:

    - It is necessary to keep the format presented in the example file. 
    - On the first line:
    	* The word "Context" needs to appear in the first column. 
    	* The word "phenotype" just above the first occurrence of the first variable that you want to observe a certain behaviour.
    
    - On the second  line you need to define variable type which are involved in the environment creation.
    		* If there is environment variable the key word to put in the first column is "ENV".
    		* For the advanced use of the tool some variables initially used to describe the phenotype can be called as  context-variable to create a specific experimentation context (Knock out, overexpression ...). In this case the variable could be in the context part. To define that this variable one stipulates in the second line starting from the column of the first occurrence of a systemic variable the word "SYS ". All others variable which belong to the same type need to apprear in the following columns. 

    -On the third line:
     the name of the set of environmental variables is indicated first under the context part, then the names of the systemic variables are put in the phenotype part (except as mentioned in the previous point). Note that the names of the systemic variables must match the variables given in the block var of the smb input file
     
     
    -On the following lines:
     	*For the context variables we will enumerate all the possible values that they can take. 
     	*For the variables of the phenotype, the key of the formula to be tested is indicated. If we do not know the expected behavior then we leave the box empty. Here you have to be careful to put an existing key on the following list :
       	 'TDS-0': "AF(AG("+x+"=0))",
                 'TDS-1': "AF(AG("+x+"=1))",
                 'TDS-2': "AF(AG("+x+"=2))",
                 '!TDS-0': "!(AF(AG("+x+"=0)))",
                 '!TDS-1': "!(AF(AG("+x+"=1)))",
                 '!TDS-2': "!(AF(AG("+x+"=2)))",
                 'OSC-1': "AG(AF("+ x + "=0) & AF(" + x + "=1 ))",
                 'OSC-2': "AG(AF(" + x + "=0) & AF(" + x + "=1 )) | AG(AF(" + x + "=0) & AF(" + x + '=2 )) | AG(AF(' + x + "=2) & AF(" + x + "=1 ))",
                 'OSC-3': "AG(AF("+ x + "=0) & AF(" + x + ">=1 )) | AG(AF("+ x + ">=0) & AF(" + x + "=2 )) | AG(AF("+ x + "=2) & AF(" + x + "=1 )) | AG(AF("+ x + "=0) & AF(" + x + "=3 )) | AG(AF("+ x + "=3) & AF(" + x + "=2 )) | AG(AF("+ x + "=3) & AF(" + x + "=1 ))",
                '!OSC-1': "!(AG(AF("+ x + "=0) & AF(" + x + "=1 )))",
                '!OSC-2': "(AG(AF(" + x + "=0) & AF(" + x + "=1 )) | AG(AF(" + x + "=0) & AF(" + x + '=2 )) | AG(AF(' + x + "=2) & AF(" + x + "=1 )))",
                'osc': "AG(AF("+ x + "=" + mi + ") & AF(" + x + "=" + ma + ")) & AF(AG("+ x +" <= " + ma + " & "+ x + " >=" + mi + ")) ", 
                'NR-0': "AG(!"+x+"=0)",
                'NR-1': "AG(!"+x+"=1)",
                'NR-2': "AG(!"+x+"=2)",
                
    	

Once all the documents have been constructed. you can luch the shell script greenbionet by using the command: >>./greenbionet smbfilepath path_to_PM 

 If there are  global models, a csvfile is written in the same path as csvfilepath input and will be named PM_name_model.csv .
 
 this tool uses inputs files to automatically creates TotemBioNet inputs files in ".smb". and obtain CSV results before doing an intersection of all abstracted models of environmental contexts. Thus if the inputs files of this tool are not well written the TotemBioNet parser raise errors. this tool gives you back the raised error from TotemBioNet parsor. To correct you're input files please refers to TotemBioNet syntaxe. and if you need to verify all cretaed files step by step they are registered in modeling-context directory either in smbfile directory or csv file directory.
 
